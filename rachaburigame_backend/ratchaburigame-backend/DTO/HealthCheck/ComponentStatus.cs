﻿using Microsoft.Extensions.Diagnostics.HealthChecks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.DTO.HealthCheck
{
    public class ComponentStatus
    {
        public string? Name { get; set; } = string.Empty;
        public string? Status { get; set; } = string.Empty;
        public string? Type { get; set; } = string.Empty;
    }
}
