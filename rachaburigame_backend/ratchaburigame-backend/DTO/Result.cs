﻿using api.Utilities.Interface;

namespace api.DTO
{
    /// <summary>
    /// Return type of the API. Note that it is declared as a generic, "Result<T>" and the data type of the "Data" property is T. It's possible to declare it as "Result" and the data type of "Data" property is "Object". The JSON result is the same for both "Result" or "Result<T>"
    /// When T is Object it means that we don't care about Data
    /// TraceId is a reference code. When the error occur, the system will generate a unique number as a TraceId. This number will be logged in the file. So, we can use this number to identify the exact exception of the reported error (e.g. there are many exception in the log file which are logged by many action from many users).
    /// </summary>
    public class Result<T>
    {
        public bool Success { get; set; } = false;
        public string Message { get; set; } = string.Empty;
        public string TraceId { get; }
        public T? Data { get; set; } = default;   //Contains data that a client need.

        public Result(ITrace trace)
        {
            TraceId = trace.GetTraceId();            
        }

        public string GetLog()
        {
            string message = string.Empty;

            message += $"Success: [{Success}]";
            message += $"Message: [{Message}]";
            message += $"TraceId: [{TraceId}]";
            message += $"Data: [{Data}]";

            return (message);
        }

        public string GetLogWithNoData()
        {
            string message = string.Empty;

            message += $"Success: [{Success}]";
            message += $"Message: [{Message}]";
            message += $"TraceId: [{TraceId}]";

            return (message);
        }
    }
}

