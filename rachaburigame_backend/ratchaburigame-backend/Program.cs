using api.Data;
// using api.Data.Interface;
// using api.DTO.HealthCheck;
// using api.HealthCheck;
// using api.Services;
// using api.Services.Interface;
using api.Utilities;
using api.Utilities.Interface;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.IdentityModel.Tokens;
using NLog;
using NLog.Web;
using System.Text;
using System.Text.Json;

var logger = LogManager.Setup().LoadConfigurationFromAppSettings().GetCurrentClassLogger();
logger.Debug("init main");

try
{    
    var builder = WebApplication.CreateBuilder(args);   
    var services = builder.Services;
    

    #region Add services

    //To use Newtonsoft Json instead of the default JSON formatter, chain AddNewtonsoftJson() to AddControllers() or AddControllersWithViews()
    services.AddControllers(); //adds support for controllers and API-related features, but not views or pages.
    //services.AddControllersWithViews(); //adds support for controllers, API-related features, and views, but not pages.
    //services.AddControllers().AddNewtonsoftJson(options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore); // Ignore looping -> https://www.newtonsoft.com/json/help/html/ReferenceLoopHandlingIgnore.htm

    // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
    services.AddEndpointsApiExplorer();
    services.AddSwaggerGen();    


    #region appsettings.json
    //https://docs.microsoft.com/en-us/aspnet/core/fundamentals/configuration/?view=aspnetcore-6.0#bind-hierarchical-configuration-data-using-the-options-pattern

    var appSettings = new AppSettings();
    builder.Configuration.GetSection(AppSettings.SectionName).Bind(appSettings);   
    //if needed, modified appSettings here for post configuration or decrypt any secrets
    services.AddTransient<AppSettings>(x => { return appSettings; });   //DI appsettings.json by using AddTransient() instead of Configure()

    /*
     * https://docs.microsoft.com/en-us/dotnet/api/microsoft.extensions.dependencyinjection.optionsservicecollectionextensions.configure?view=dotnet-plat-ext-6.0
     * 
     * How to inject appsettings.json by using Configure() method (see .net core 3.0 version of base for example)
     * However, it doesn't work with nested value in Azure
     * But this method works with PostConfigure() (https://docs.microsoft.com/en-us/dotnet/api/microsoft.extensions.dependencyinjection.optionsservicecollectionextensions.postconfigure?view=dotnet-plat-ext-6.0) 
     *      1. To simulate this behavior, manually override configuration value (post configure) after reading it from appsettings.json (Bind())
     *      2. If I use PostConfigure(), I can't access overrided value in Program.cs (see comment in .net core 3.0 version of base for more details)
     *
     */

    #endregion

    #region Database
    services.AddDbContext<ApiContext>(options =>
    {
        //If needed, decrypt connection string here
        string connectionString = builder.Configuration.GetConnectionString("databaseConnection");

        //for SQL Server
        options.UseSqlServer(connectionString);

        //for MySQL
        // db.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString));
    });
    #endregion

    #region Health check
    /*
     * Uncomment below section to add health check to the project
     *      1. AddHealthChecks()
     *      2. MapHealthChecks()
     */

    //services.AddHealthChecks()
    //    .AddDbContextCheck<ApiContext>("Database")
    //    .AddCheck<EmailHealthCheck>("Email");
    #endregion

    #region Dependency Injection    
    //Use the right method that suit your object life time i.e. AddTransient, AddScoped, AddSingleTon.    
    //https://docs.microsoft.com/en-us/aspnet/core/fundamentals/dependency-injection?view=aspnetcore-6.0#lifetime-and-registration-options

    //All other classes except service and repo are defined here for DI
    services.AddHttpContextAccessor();  //IHttpContextAccessor to any classes that declare it as a dependency in their constructors
    services.AddTransient<ITrace, Trace>();
    services.AddTransient<IJwtService, JwtService>();
    //services.AddTransient<IEmail,Email>();

    #region Services
    //All project services are added here for dependency injection
    // services.AddTransient<ISampleService, SampleService>();
    #endregion

    #region Repositories
    //All project repositories are added here for dependency injection
    // services.AddTransient<ISampleRepository, SampleRepository>();
    #endregion

    #endregion

    #region JWT
    //Configure Role Based Authorization: https://jasonwatmore.com/post/2019/10/16/aspnet-core-3-role-based-authorization-tutorial-with-example-api
    //TODO: jwtKey must be a decrypted version. If PostConfigure() is used for decrypting and Configure() is used for DI, decrypt jwt again here. Otherwise, decrypt it right after Bind() before calling AddTransient()
    var jwtKey = Encoding.ASCII.GetBytes(appSettings.Jwt.Secret);
    services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                options.Events = new JwtBearerEvents
                {
                    OnTokenValidated = context =>
                    {
                        //If needed, insert additional logic here
                        /*
                        var activeDirectory = context.HttpContext.RequestServices.GetRequiredService<IActiveDirectory>();
                        var samAccountName = context.Principal.Identity.Name;
                        User user;

                        if (!activeDirectory.UserExists(samAccountName, out user))
                        {
                            // return unauthorized if user no longer exists
                            context.Fail("Unauthorized");
                        }
                        */

    return Task.CompletedTask;
                    }
                };
                options.RequireHttpsMetadata = false;
                options.SaveToken = true;
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(jwtKey),
                    ValidateIssuer = false,
                    ValidateAudience = false
                    //Set ClockSkew if you really want a token to be expired at the exact time
                    //https://stackoverflow.com/questions/43045035/jwt-token-authentication-expired-tokens-still-working-net-core-web-api
                    //ClockSkew = TimeSpan.Zero
                };
            });
    #endregion
    
    #endregion

    // NLog: Setup NLog for Dependency injection
    builder.Logging.ClearProviders();
    builder.Host.UseNLog();

    
    var app = builder.Build();

    // Configure the HTTP request pipeline.
    if (app.Environment.IsDevelopment())
    {
        app.UseSwagger();
        app.UseSwaggerUI();    
    }

    /*
     * Register Exception Handling Middleware in non-dev environment to forward an unhandle exception to route "/error". Then need to setup a Controller for this route
     * Note that Exception Handling Middleware does the following for us automatically
     *      1. Catches and logs exceptions.
     *      2. Re-executes the request in an alternate pipeline for the page or controller indicated. The request isn't re-executed if the response has started.
     * 
     */    
    app.UseExceptionHandler("/error");

    app.UseHttpsRedirection();
    // app.UseStaticFiles(); //Call this before UseRouting()

    //CORS policy
    //TODO: Uncomment below when you cannot connect this api because of CORS Policy
    // app.UseCors(x => x
    //     .AllowAnyOrigin()
    //     .AllowAnyMethod()
    //     .AllowAnyHeader());

    app.UseAuthentication();
    app.UseAuthorization();
    app.MapControllers();    
    //app.MapHealthChecks("/health", new HealthCheckOptions()
    //{
    //    // This custom writer formats the detailed status as JSON.
    //    ResponseWriter = JsonHealthCheckResponseWriter
    //});
    app.Run();
}
catch (Exception exception)
{
    // NLog: catch setup errors
    logger.Error(exception, "Stopped program because of exception");
    throw;
}
finally
{
    // Ensure to flush and stop internal timers/threads before application-exit (Avoid segmentation fault on Linux)
    LogManager.Shutdown();
}

// for reference
// https://docs.microsoft.com/en-us/aspnet/core/host-and-deploy/health-checks?view=aspnetcore-6.0
Task JsonHealthCheckResponseWriter(HttpContext context, HealthReport result)
{
    IWebHostEnvironment env = context.RequestServices.GetRequiredService<IWebHostEnvironment>();
    string json = string.Empty;

    context.Response.ContentType = "application/json; charset=utf-8";

    ServerStatus serverStatus = new ServerStatus { EnvironmentName = env.EnvironmentName };
    //serverStatus.Status = Enum.GetName(typeof(HealthStatus), result.Status);
    serverStatus.Server = new ComponentStatus
    {
        Name = "BaseRestAPI v6.0",//env.ApplicationName, 
        Status = Enum.GetName(typeof(HealthStatus), result.Status),
        Type = Enum.GetName(typeof(ComponentType), ComponentType.Server)
    };

    foreach (var entry in result.Entries)
    {
        ComponentStatus dependencyStatus = new ComponentStatus
        {
            Name = entry.Key,
            Status = Enum.GetName(typeof(HealthStatus), entry.Value.Status),
            Type = Enum.GetName(typeof(ComponentType), ComponentType.Component)
        };

        serverStatus.Dependencies.Add(dependencyStatus);
    }

    json = JsonSerializer.Serialize(serverStatus);

    return context.Response.WriteAsync(json);
}   