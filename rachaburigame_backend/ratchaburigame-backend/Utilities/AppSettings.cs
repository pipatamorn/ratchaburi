﻿namespace api.Utilities
{
    public class AppSettings
    {
        public const string SectionName = "AppSettings";

        public Smtp Smtp { get; set; } = default!;
        public Jwt Jwt { get; set; } = default!;
        public ErrorMessage ErrorMessage { get; set; } = default!;
        public HealthCheck HealthCheck { get; set; } = default!;
    }

    public class Smtp
    {
        public string Name { get; set; } = string.Empty;
        public string Server { get; set; } = string.Empty;
        public int Port { get; set; }
        public bool RequireAuthentication { get; set; }
        public bool EnableSsl { get; set; }
        public string Username { get; set; } = string.Empty;
        public string Password { get; set; } = string.Empty;
    }

    public class Jwt
    {
        public string Secret { get; set; } = string.Empty;
        public int ExpireInSec { get; set; }
    }
    public class ErrorMessage
    {
        public string General { get; set; } = String.Empty;
        public string EmptyUsername { get; set; } = String.Empty;
        public string EmptyPassword { get; set; } = String.Empty;
    }

    public class HealthCheck
    {
        public HealthCheckEmail HealthCheckEmail { get; set; } = default!;
    }

    public class HealthCheckEmail
    {
        public string FromAddress { get; set; } = string.Empty;
        public string FromDisplayName { get; set; } = string.Empty;
        public string Subject { get; set; } = string.Empty;
        public string Recipient { get; set; } = string.Empty;
        public string Body { get; set; } = string.Empty;

    }
}
