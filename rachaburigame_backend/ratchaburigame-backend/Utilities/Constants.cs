namespace api.Utilities
{
    public static class Constant
    {
        public static class Role
        {
            //Value of all roles should be the same as in the database
            public const string Admin = "Admin";
            public const string User = "User";
        }

        public static class Jwt
        {
            public const string Header = "Authorization";
        }
    }
}
