using System.Runtime.InteropServices;

namespace api.Utilities.Interface
{
    public interface IJwtService
    {
        string GenerateJwtTokenString(string uniqueName, [Optional] List<string> roles);
        string? GetClaimValue(HttpRequest httpRequest, string type);
    }
}
