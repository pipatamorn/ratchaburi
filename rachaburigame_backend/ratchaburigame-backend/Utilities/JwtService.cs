using api.Utilities.Interface;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Runtime.InteropServices;
using System.Security.Claims;
using System.Text;

namespace api.Utilities
{
    public class JwtService : IJwtService
    {
        private AppSettings AppSettings { get; }

        public JwtService(AppSettings appSettings)
        {
            AppSettings = appSettings;
        }

        public string? GetClaimValue(HttpRequest httpRequest, string type)
        {
            /*
             * Example value of httpRequest.Headers[Constant.Jwt.Header].ToString()
             * "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiYXVzZXJuYW1lIiwicm9sZSI6IkFkbWluIiwibmJmIjoxNjYxNjc0MTczLCJleHAiOjE2NjE3NjA1NzMsImlhdCI6MTY2MTY3NDE3M30.2pXP3wQqz9JeBKW58xkyKEyndMRvOFCYXTgjnLAT8tI"           
             * 
             * "type" should be a shortname version of ClaimTypes (URI) e.g.
             *      ClaimTypes.Name is "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name"
             *      "type" should be "name"
             */
            var tokenString = httpRequest.Headers[Constant.Jwt.Header].ToString().Split()[1];
            var handler = new JwtSecurityTokenHandler();
            var jsonToken = handler.ReadToken(tokenString) as JwtSecurityToken;            
            string? value = jsonToken?.Claims.FirstOrDefault(c => c.Type == type)?.Value;

            return value;
        }

        //https://jasonwatmore.com/post/2019/10/16/aspnet-core-3-role-based-authorization-tutorial-with-example-api
        public string GenerateJwtTokenString(string username, [Optional] List<string> roles)
        {
            var tokenHandler = new JwtSecurityTokenHandler();            
            var jwtKey = Encoding.ASCII.GetBytes(AppSettings.Jwt.Secret);
            var claims = new List<Claim>();

            claims.Add(new Claim(ClaimTypes.Name, username));            
            foreach (var role in roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role));
            }

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddSeconds(((double)AppSettings.Jwt.ExpireInSec)),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(jwtKey), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);

            return tokenString;
        }
    }
}
