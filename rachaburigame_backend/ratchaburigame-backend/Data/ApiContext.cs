﻿using api.Models;
using Microsoft.EntityFrameworkCore;

/// <summary>
/// DbContext represent database
/// Each table is listed as property of DbContext class
/// To migrate database (update/delete/create table on the database) (https://docs.microsoft.com/en-us/ef/core/managing-schemas/migrations/?tabs=vs)
///     Open package manager contole by selecting Tools -> NuGet Package Manager -> Package Manager Console
///     Run the following command
///         Add-Migration "Your change description"
///             This command will create migration script (SQL script) from code (model) changes
///         Update-Database
///             To apply all pending migration scripts to the database
/// </summary>
namespace api.Data
{
    /// <summary>
    /// So, as long as you let DbContext manage your connections, feel free to ignore the Dispose method. On the other hand, if you're managing your Connections, the Dispose method may be your bestest friend.
    /// </summary>
    public class ApiContext : DbContext
    {
        /// <summary>
        /// Need to implement this constructor to accept options passed from DI container
        /// https://docs.microsoft.com/en-us/ef/core/miscellaneous/configuring-dbcontext#using-dbcontext-with-dependency-injection
        /// </summary>
        /// <param name="options"></param>
        public ApiContext(DbContextOptions<ApiContext> options) : base(options)
        {
        }

        //List of tables here
        public DbSet<Sample> Samples { get; set; } 


        //Below code if I want to configure DbContext internally
        //protected override void OnConfiguring(DbContextOptionsBuilder options) => options.UseSqlite("Data Source=PasswordReset.db");
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

        }
    }
}
